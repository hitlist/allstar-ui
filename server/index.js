'use strict'

const Hapi = require('hapi')
const Boom = require('boom')
const conf = require('keef')
const HapiNuxt = require('hapi-nuxt')
const engine = require('catbox-redis')
const {graphqlHapi, graphiqlHapi} = require('apollo-server-hapi')
const log = require('../lib/log')
const SCHEMA = require('./graphql/schema')

const host = conf.get('host')
const port = conf.get('port')
const server = new Hapi.Server({ port, host })
const hemera = require('../lib/hemera')

const cookie_opts = {
  ttl: 365 * 24 * 60 * 60 * 1000, // expires a year from today
  encoding: 'none',    // we already used JWT to encode
  isSecure: false,      // warm & fuzzy feelings
  isHttpOnly: false,    // prevent client alteration
  clearInvalid: false, // remove invalid cookies
  strictHeader: true,  // don't allow violations of RFC 6265
  path: '/'            // set the cookie for all routes
}

;(async () => {
  await hemera.ready()
  await server.register(require('hapi-auth-jwt2'))
  await server.register({
    plugin: HapiNuxt
  , options: require('../nuxt.config')
  })


  await server.initialize()
  await server.cache.provision({ engine, name: 'sessions'})

  server.state(conf.get('session:cookie'), cookie_opts)
  const cache = server.cache({
    segment: 'sessions'
  , expiresIn: 1000 * 60 * 5
  , cache: 'sessions'
  })

  server.app.cache = cache
  server.app.hemera = hemera

  server.auth.strategy('jwt', 'jwt', {
    cookieKey: conf.get('session:cookie')
  , key: conf.get('session:secret')
  , verifyOptions: { algorithms: [ 'HS512' ] }
  , validate: async (decoded, request) => {
      const cached = await request.server.app.cache.get(request.auth.token)
      if (!cached) {
        await request.server.app.cache.drop(request.auth.token)
      }
      log.debug('touch session')
      request.server.app.cache.set(request.auth.token, cached)
      const ret =  {isValid: !!cached}
      return ret
    }
  })

  server.route(require('./routes'))

  if (conf.get('node_env') !== 'production') {
    log.debug('loading debug interface')
    await server.register({
      plugin: graphiqlHapi
    , options: {
        path: '/debug'
      , graphiqlOptions: {
          endpointURL: '/graphql'
        }
      }
    })
  }
  await server.register({
    plugin: graphqlHapi
  , options: {
      path: '/graphql'
    , graphqlOptions: (request) => {
        log.info(request.method)
        log.debug('grapql request authenticated', request.auth.isAuthenticated)
        const user = request.auth.isAuthenticated
          ? request.auth.credentials
          : {}
        return {
          schema: SCHEMA
        , pretty: true
        , context: {user}
        }
      }
    , route: {
        cors: true
      , auth: {
          mode: 'try'
        , strategy: 'jwt'
        }
      }
    }
  })
  await server.start()

  process.once('SIGTERM', onSignal)
  process.once('SIGINT', onSignal)
})().then(() => {
  console.log(`Server running at: ${server.info.uri}`)
})
.catch((err) => {
  console.log(err)
})

function onSignal() {
  log.info('shutdown signal received')
  log.info('shutdown hemera')
  hemera.close(() => {
    log.info('hemera closed')
    server.stop().then(() => {
      log.info('server closed')
      process.exit(0)
    })
  })
}
