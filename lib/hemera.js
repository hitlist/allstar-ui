
'use strict'
/**
 * module to boot strap hemera
 * @module allstar-auth/lib/hemera
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires keef
 * @requires bitters
 * @requires nats
 * @requires nats-hemera
 * @requires @allstar/parse-hosts
 * @requires @allstar/hemera-acl
 **/
const conf = require('keef')
const log = require('./log')
const nats = require('nats')
const Hemera = require('nats-hemera')
const hemeraJaeger = require('hemera-jaeger')
const stats = require('hemera-stats')
const parse = require('@allstar/parse-hosts')
const acl = require('@allstar/hemera-acl')

const nats_config = conf.get('nats')

const connection = {
  ...nats_config
, servers: parse(nats_config.servers)
}

log.debug('nats config', connection)

const nc = nats.connect(connection)

const hemera = new Hemera(nc, {
  logLevel: 'trace'
, childLogger: true
, tag: 'auth'
})

hemera.use(hemeraJaeger, conf.get('jaeger'))
hemera.use(stats)
hemera.use(acl)

module.exports = hemera

