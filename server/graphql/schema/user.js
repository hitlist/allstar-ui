'use strict'

const Name = require('./name')
module.exports = `

type User {
  username: String
  created: DateTime
  email: Email
  permissions: JSON
  superuser: Boolean
  active: Boolean
  roles: [String]
  name: Name
  pk: UUID
  allstar_auth_user_id: ID!
}
`
