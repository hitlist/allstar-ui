'use strict'

const crypto = require('crypto')
const Boom = require('boom')
const JWT = require('jsonwebtoken')
const conf = require('keef')
const log = require('../../../lib/log')
module.exports = {
  method: 'POST'
, path: '/session'
, options: {
    cors: true
  , handler: async (request, h) => {
      const hemera = request.server.app.hemera
      const {email, password} = request.payload
      try {
        var resp = await hemera.act({
          topic: 'user'
        , cmd: 'login'
        , version: 'v1'
        , email: email
        , password: password
        })

      } catch (err) {
        console.error(err)
        return Boom.unauthorized()
      }

      const user = resp.data
      try {
        var sid = JWT.sign(user, conf.get('session:secret'), {
          algorithm: 'HS512'
        })
      } catch (err) {
        log.error(err)
      }

      log.info('log in success', email)
      await request.server.app.cache.set(sid, {user})
      h.state(conf.get('session:cookie'), sid)
      return user
    }
  }
}
