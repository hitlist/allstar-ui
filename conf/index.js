'use strict'

const PRODUCTION = process.env.NODE_ENV === 'production'

module.exports = {
  port: 3001
, host: '0.0.0.0'
, pino: {
    prettyPrint: !PRODUCTION
  , extreme: PRODUCTION
  , level: 'trace'
  }
, nats: {
    servers: [
      "nats://0.0.0.0:4222"
    , "nats://0.0.0.0:4223"
    , "nats://0.0.0.0:4224"
    ]
  , user: null
  , pass: null
  }
, redis: {
    host: 'redis://0.0.0.0:6379'
  }
, session: {
    cookie: '_alm'
  , secret: null
  , secure: false
  }
, jaeger: {
    serviceName: 'auth'
  , delegateTags: [{
      key: 'op'
    , tag: 'allstar.db.query'
    }]
  , jaeger: {
      sampler: {
        type: 'Const',
        options: true
      },
      options: {
        tags: {
          'hemera.version':`Node-${require('nats-hemera/package.json').version}`
        , 'nodejs.version': process.versions.node
        }
      },
      reporter: {
        host: 'localhost'
      }
    }
  }
}
