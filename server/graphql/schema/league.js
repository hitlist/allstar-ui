'use strict'

const GraphQLJSON = require('graphql-type-json')
const CustomTypes = require('graphql-custom-types')

module.exports = `
type League {
  abbr: String
  """List of founding members"""
  founders: [User]
  created_at: DateTime
  league_name: String
  allstar_leauge_id: ID!
}
`
