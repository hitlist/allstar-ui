'use strict'

const GraphQLJSON = require('graphql-type-json')
const Tools = require('graphql-tools')
const CustomTypes = require('graphql-custom-types')
const User = require('./user')
const Name = require('./name')
const League = require('./league')

const {
  makeExecutableSchema
, mergeSchemas
} = Tools

const {
  GraphQLEmail
, GraphQLUUID
, GraphQLDateTime
} = CustomTypes

const Query = `
scalar Email
scalar UUID
scalar DateTime
scalar JSON

type Query {
  user(pk: Email): User
  users(first_name: String): [User]
  leagues: [League]
}
`;

const Schema = `
schema {
  query: Query
}
`

module.exports = makeExecutableSchema({
  typeDefs: [
    Schema
  , Query
  , User
  , Name
  , League
  ]
, resolvers: require('../resolvers')
})
