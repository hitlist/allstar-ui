'use strict'

module.exports = `
type Name {
  first: String
  last: String
}
`
