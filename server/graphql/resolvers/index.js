'use strict'
const GraphQLJSON = require('graphql-type-json')
const CustomTypes = require('graphql-custom-types')
const hemera = require('../../../lib/hemera')

const {
  GraphQLEmail
, GraphQLUUID
, GraphQLDateTime
} = CustomTypes

module.exports = {
  JSON: GraphQLJSON
, Email: GraphQLEmail
, DateTime: GraphQLDateTime
, UUID: GraphQLUUID
, Query: {
    async users(_, args, ctx, query) {

      const resp = await hemera.act({
        topic: 'user'
      , cmd: 'list'
      , version: 'v1'
      , meta$: ctx
      })

      const users = resp.data.data
      return users
    }
  , async user(_, args) {
      if (!args.pk) {
        const err = new Error('Must specify a user id (pk) to look up')
        err.name = err.code = 'EBADREQUEST'
        throw err
      }
      const resp = await hemera.act({
        topic: 'user'
      , cmd: 'get'
      , version: 'v1'
      , user_id: args.pk
      , meta$: {
          user: {
            superuser: true
          }
        }
      })

      return resp.data
    }
  , async leagues(_, args, ctx, query) {
      const resp = await hemera.act({
        topic: 'league'
      , version: 'v1'
      , cmd: 'list'
      , meta$: ctx
      })

      return resp.data.data
    }
  }
}
